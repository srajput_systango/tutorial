def india_tax(price)
	if price<=100
  		sales_tax=0
  	elsif price>100 && price<=500
     		sales_tax= (price-100)* 0.05
  	elsif price>500
  		sales_tax= (price-500)*0.2+ 400*0.05
  	end
	return sales_tax
end

def usa_tax(price)
	sales_tax=Math.sqrt(price)
	return sales_tax
end

def uk_tax(price)
	sales_tax=(price)*0.03
	return sales_tax
end

def other_tax(price)
	sales_tax=" program doesn't support your country "
	return sales_tax
end

def calculate_sales_tax(country,price)
	if country=="India"
 		sales_tax=india_tax(price)
  	elsif country=="USA"
  		sales_tax=usa_tax(price)
  	elsif country=="UK"
  		sales_tax=uk_tax(price)
  	else
  		sales_tax=other_tax(price)
  	end
	return sales_tax
end

require 'csv'

CSV.open("output.csv","w+b") do |csv|

csv<<["Price","Country","Sales Tax"]

data=CSV.foreach('input.csv',headers:true) do |row|

	temporary_country_value=row['Country']
	t_c_v=temporary_country_value
	
	temporary_price_value=row['Price'].to_i
	t_p_v=temporary_price_value
	
	sales_tax=calculate_sales_tax(t_c_v,t_p_v)

	csv<<[t_p_v,t_c_v,sales_tax]
end
end

