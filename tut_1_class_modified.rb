require 'csv'

class InputReader
	
	def initialize(file_name)
		@file_name=file_name
	end

	def check_file
		if File.exists?(@file_name)
			return true
		else
			puts "File doesn't exists,Program is Terminating"
			exit
		end
	end

	def data_extract_file
		@data=CSV.read(@file_name,headers:true)
		return @data
	end
end

class DataGenerate

	def initialize(data)
		@data=data
	end

	def data_generate
		@price_array=Array.new
		@country_array=Array.new
		$i=0
		$limit=@data.length

		@price_column=0
		@country_column=1

		while $i<$limit do

			if /^(?<num>\d+)$/ =~ @data[$i][@price_column]			#to check whether the given string is numeric or not
				@price_array[$i]=@data[$i][@price_column].to_i
			else
				@price_array[$i]="Bad Data"
			end
			
			if /^(?<num>\d+)$/ =~ @data[$i][@country_column] or  @data[$i][@country_column]==nil  #to check whether the given string is word or not
				@country_array[$i]="Bad Data"
			else
				@country_array[$i]=@data[$i][@country_column]
			end

			$i+=1
		end
	end

	def data_price
		return @price_array
	end

	def data_country
		return @country_array
	end

	def data_number
		return @data.length
	end

end

class DataContainer
	def initialize(price,country)
		@price_storage=price
		@country_storage=country
	end

	def data_required_price(i)
		return @price_storage[i]
	end

	def data_required_country(i)
		return @country_storage[i]
	end
end

class Calculator
	def initialize(price,country)
		@price_temporary=price
		@country_temporary=country
	end

	def calculate
		if @country_temporary==nil
			sales_tax=0
		elsif @country_temporary=="Bad Data"
			sales_tax="Sales Tax can't be Computed, Bad Data"
		elsif @country_temporary.casecmp("India")==0
 			sales_tax=india_tax_slab()
  		elsif @country_temporary.casecmp("USA")==0
  			sales_tax=usa_tax_slab()
  		elsif @country_temporary.casecmp("UK")==0
  			sales_tax=uk_tax_slab()
  		else
  			sales_tax=other_tax_slab()
  		end
		return sales_tax
	end

	def india_tax_slab
		if @price_temporary=="Bad Data"
			sales_tax="Sales Tax can't be Computed, Bad Data"
		elsif @price_temporary<=100
  			sales_tax=0
  		elsif @price_temporary>100 && @price_temporary<=500
     		sales_tax= (@price_temporary-100)* 0.05
  		elsif @price_temporary>500
  			sales_tax= (@price_temporary-500)*0.2+ 400*0.05
  		end
		return sales_tax
	end

	def usa_tax_slab
		if @price_temporary=="Bad Data"
			sales_tax="Sales Tax can't be Computed, Bad Data"
		else
			sales_tax=Math.sqrt(@price_temporary)
		end
		return sales_tax
	end

	def uk_tax_slab
		if @price_temporary=="Bad Data"
			sales_tax="Sales Tax can't be Computed, Bad Data"
		else
			sales_tax=(@price_temporary)*0.03
		end
		return sales_tax
	end

	def other_tax_slab
		if @price_temporary=="Bad Data"
			sales_tax="Sales Tax can't be Computed, Bad Data"
		else
			sales_tax=" Program doesn't Support Your Country "
		end
		return sales_tax
	end
end

class Item
	
	@@count=0
	@@new_data_array=Array.new

	def initialize(price,country)
		@price=price
		@country=country
	end

	def process()
		h=Calculator.new(@price,@country)
		@sales_tax=h.calculate()
	end

	def data_to_array
		@@new_data_array[@@count]=[@price,@country,@sales_tax]
		
		@@count+=1
	end

	def modified_array
		return @@new_data_array
	end
end

class OutputWriter
	def initialize(output_file_name,modified_array)
		@output_file_name=output_file_name
		@modified_array=modified_array
	end

	def data_write
		CSV.open(@output_file_name,"w+b") do |csv|
			csv<<["Price","Country","Sales Tax"]

			$i=0
			$limit=@modified_array.length
			
			while $i<$limit do
				csv<<@modified_array[$i]

				$i+=1
			end
		end
	end
end



read_data=InputReader.new("input.csv")
check=read_data.check_file
data_array=read_data.data_extract_file


data_separate=DataGenerate.new(data_array)
data_separate.data_generate()
price_array=data_separate.data_price()
country_array=data_separate.data_country()
$limit=data_separate.data_number()


data_storage=DataContainer.new(price_array,country_array)

$i=0

while $i<$limit do
	
	item_number=$i

	temporary_price=data_storage.data_required_price(item_number)
	temporary_country=data_storage.data_required_country(item_number)

	item=Item.new(temporary_price,temporary_country)
	item.process()
	item.data_to_array()

	$i+=1
end

array_to_write=item.modified_array()

data_writer=OutputWriter.new("output_class_modified.csv",array_to_write)
data_writer.data_write()