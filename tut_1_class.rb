require 'csv'

class Item
	
	def check_file(file_name)
		if File.exists?(file_name)
			return true
		else
			return false
		end
	end


	def extract_data(check,file_name)
		if check==true
			data=CSV.read(file_name,headers:true)
			return data
		else
			puts "File doesn't Exists, Program is Terminating"
			exit
		end
	end


	def data_generate(data,i)
		
		column_price=0
		column_country=1

		return data[i][column_price].to_i,data[i][column_country]
	end

	def tax_calculator(data_row)
		
		column_price=0
		column_country=1

		price=data_row[column_price].to_i
		country=data_row[column_country]
		
		if country==nil
			sales_tax=0
		elsif country.casecmp("India")==0
 			sales_tax=india_tax_slab(price)
  		elsif country.casecmp("USA")==0
  			sales_tax=usa_tax_slab(price)
  		elsif country.casecmp("UK")==0
  			sales_tax=uk_tax_slab(price)
  		else
  			sales_tax=other_tax_slab(price)
  		end
		
		return sales_tax
	end

	def india_tax_slab(price)
		if price<=100
  			sales_tax=0
  		elsif price>100 && price<=500
     		sales_tax= (price-100)* 0.05
  		elsif price>500
  			sales_tax= (price-500)*0.2+ 400*0.05
  		end
		
		return sales_tax
	end

	def usa_tax_slab(price)
		sales_tax=Math.sqrt(price)
		return sales_tax
	end

	def uk_tax_slab(price)
		sales_tax=(price)*0.03
		return sales_tax
	end

	def other_tax_slab(price)
		sales_tax=" Program doesn't Support Your Country "
		return sales_tax
	end

	def data_write(check,array_to_write,output_file_name)
		CSV.open(output_file_name,"w+b") do |csv|
			csv<<["Price","Country","Sales Tax"]
			
			if check==true
				$i=0
				$limit=array_to_write.length

				while $i<$limit do
					csv<<array_to_write[$i]

					$i+=1
				end
			end
		end
	end
end

input_file_name="input.csv"
output_file_name="output_class.csv"

tax=Item.new

check=tax.check_file(input_file_name)
data=tax.extract_data(check,input_file_name)

$i=0
$limit=data.length

modified_data_array=Array.new

column_tax=2

while $i<$limit do
	
	$data_temporary=tax.data_generate(data,$i)

	sales_tax=tax.tax_calculator($data_temporary)
	
	$data_temporary[column_tax]=sales_tax
	
	modified_data_array[$i]=$data_temporary
	
	$i+=1
end

tax.data_write(check,modified_data_array,output_file_name)