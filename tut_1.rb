
require 'csv'

CSV.open("output.csv","w+b") do |csv|

csv<<["Price","Country","Sales Tax"]

data=CSV.foreach('input.csv',headers:true) do |row|

country=row['Country']

price=row['Price'].to_i

	if country=="India"
 		if price<=100
  			tax=0
  		elsif price>100 && price<=500
     			tax= (price-100)* 0.05
  		elsif price>500
  			tax= (price-500)*0.2+ 400*0.05
  		end
  	elsif country=="USA"
  		tax=Math.sqrt(price)
  		
  	elsif country=="UK"
  		tax=(price)*0.03
  	else
  		tax=" This Program Doesn't Support #{country}"
  	end

csv<<[price,country,tax]
end
end

